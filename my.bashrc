alias ll='ls -alF'
alias la='ls -A'
alias l='ls -CF'
alias desktop="cd ~/Bureau"
alias cdb="cd ~/Bureau"
alias o=xdg-open 

if [ "$color_prompt" = yes ]; then
    PS1='${debian_chroot:+($debian_chroot)}\[\033[01;32m\]\u@\h\[\033[00m\]:\[\033[01;36m\]\w\[\033[00m\]\$ '
else
    PS1='${debian_chroot:+($debian_chroot)}\u@\h:\w\$ '
fi

alias python='/usr/bin/python3'