#=====================================================================
#titre:			mkscript.sh
#auteur: 		William Colpaert (william.colpaert.etu@univ-lille.fr)
#date: 			22/10/2020
#description: 		Script qui crée un script et son header automatiquement ou avec des arguments
#version:		0.1
#note:			Syntax: mkscript.sh [FILE] 	[-p|-h]		[-m|-a|-d|-v "<argument>"]
#				Default usage: mkscript.sh file.sh  	OR	mkscript.sh /path/to/file.sh
#				Renomme automatiquement le fichier sans l'extension .sh
#				Fonctionne avec un mode mkdir -p pour créer les parents manquant du path du file
#			Options:
#			obtenir de l'aide: -h OR --help
#			utiliser une autre email: -m "<mail@mail.com>"
#			renseigner un auteur différent: -a "<auteur>"
#			description: -d "<description>"
#			choisir une version: -v "<0.1>"
#			se mettre en mode mkdir -p : -r
#=====================================================================


## DECLARATION DES VARIABLES GLOBALES ###

DATE=$(date +'%d/%m/%Y')
USERNAME=$(getent passwd | grep $USER | cut -d":" -f5)
USERMAIL=$(echo $USERNAME | tr [[:upper:]] [[:lower:]] | tr " " ".")".etu@univ-lille.fr"
ARG_PATH=$1


## FUNCTION ##
#############################################
#Affiche l'aide dans le terminal.
#Globals:
#	None
#Aguments
#	None
#Outputs:
#	Affiche l'aide du script dans le stdout
#############################################
get_help() {

	echo 'Syntax: mkscript.sh [file.sh | file | path/file.sh]     [-p|-h]         [-m|-a|-d|-v "<argument>"]'
	echo "Default usage: mkscript.sh file.sh      OR      mkscript.sh /path/to/file.sh"
	echo "Options:"
	echo -e "-h OR --help \t\t obtenir de l'aide"
	echo -e "-m \"<mail>\"\t\t utiliser un autre mail"
	echo -e "-d \"<description>\"\t description"
	echo -e "-v \"<version>\"\t\t choisir une version"
	echo -e "-r \t\t\t se mettre en mode mkdir -p"

}

## MAIN ##

# CONTROLE DES FLAGS #

if [[ $# -eq 0 ]]; then			#Il faut spécifié au moins le path complet ou le filename
	echo 'Veuillez saisir des arguments'
	get_help
	exit 1
fi


if [[ $# -ge 2 ]]; then
	shift
	while getopts ":a:m:d:v:r" opt; do
		case $opt in
			a)
				USERNAME=$OPTARG
			;;
			m)
				USERMAIL=$OPTARG
			;;
			d)
				description=$OPTARG
			;;
			v)
				version=$OPTARG
			;;
			r)
				recursif_mode="true"
			;;
			\?)
				echo "Option Invalide: $OPTARG" >&2
				exit 2
			;;
			:)
				echo "Option -$OPTARG require un argument"
				exit 3
			;;
		esac
	done
fi


while getopts ":-help:h:" opt; do
	case $opt in
		h | --help)
			get_help
			exit 0
		;;
		\?)
			echo 'Mauvaise usage, veuillez lire le help'
			get_help
			exit 1
		;;
	esac
done


# CREATION DU FICHIER SCRIPT #

file=$(basename $ARG_PATH)
parentdir=$(dirname $ARG_PATH)

if [[ ! $parentdir == ""  ]]; then										#Obligatoire pour mieux gérer les chemins
	parentdir=$parentdir/
fi

script_full_path=$parentdir$file


if [[ (! -d $parentdir) && (! $parentdir == "") && ( $recursif_mode == "true") ]]; then				#Si option -p on créer les répertoires récursifs
	mkdir -p $parentdir
elif [[ (! -d $parentdir) && (! $parentdir == "") && (! $recursif_mode == "true") ]]; then			#Si l'option -p est pas activé mais qu il manque des répertoire on demande à l utilisateur si il vaut le récursif
	read -p 'Vous voulez vous créer les répertoires manquants (Y/n) ?: ' reply
	if [[ $reply =~ ^(([yY][eE][sS]|[yY])|([oO][uU][iI]|[oO]))$ ]]; then
		mkdir -p $parentdir
		echo " répertoire $parendir créé"
	else
		echo "exit 3"
		exit 3
	fi
fi


if [[ ! -e $script_full_path ]]; then		#On vérfie si y a pas un fichier ou un dossier qui existe avec le path
	touch $script_full_path
else
	echo "le chemin $script_full_path spécifié est soit un répertoire ou soit un fichier déjà existant"
	exit 2
fi



if [[ ! $file =~ \.sh$ ]]; then		#On ajoute extension sh si le fichier n'est pas
	echo "le fichier $file va avoir sera designé sous l'appelation $file.sh"
	mv $script_full_path $script_full_path.sh
	script_full_path=$script_full_path.sh
fi

chmod u+x $script_full_path


# CREATION DU HEADER DANS LE FICHIER SCRIPT #

echo '#=======================================================================================' >> $script_full_path
echo -e "#titre: \t\t$(basename $script_full_path)" >> $script_full_path
echo -e "#auteur: \t\t$USERNAME ($USERMAIL)" >> $script_full_path
echo -e "#date: \t\t\t$DATE" >> $script_full_path
echo -e "#description: \t\t$description" >> $script_full_path
echo -e "#version: \t\t$version" >> $script_full_path
echo -e "#note: \t\t\t" >> $script_full_path
echo '#=======================================================================================' >> $script_full_path