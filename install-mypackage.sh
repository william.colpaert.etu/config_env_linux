#=======================================================================================
#titre:                 main.sh
#auteur:                William COLPAERT (william.coplaert.etu@univ-lille.fr)
#date:                  01/11/2020
#description:           Script qui install mes packets utils
#version:               0.1
#note:
#=======================================================================================

apt update && apt upgrade -y
apt install tree -y
apt install dnsutils -y
apt install ldap-utils -y
